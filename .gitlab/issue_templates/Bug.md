<!-- If you want, you can donate here (http://bit.ly/2tXWnDQ) -->
<!-- I thank all support and users of the CoinBlockerLists. -->

### Description:

*Replace this line with a brief summary of the problem.*

### Steps To Reproduce:

1.
2.
3.

### Log Files and Pages Source code:

Note: Please use Hastebin.com for source code, this makes my work easier.
https://hastebin.com/

### URL and IPlists:

Note: Please make domains and IP addresses always unclickable.

```
0.0.0.0
cryptojackingdomain.com
```


### Additional Information:
